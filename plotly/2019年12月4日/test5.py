import pandas as pd
import seaborn as sns

sns.set()

sns.set(style="white")

# Load the example mpg dataset
mpg = sns.load_dataset("mpg")

# Plot miles per gallon against horsepower with other semantics
# 其中x,y为横轴坐标变量，hue表示分类类别，size表示点的大小
sns.relplot(x="horsepower", y="mpg", hue="origin", size="weight",
            sizes=(40, 400), alpha=.5, palette="muted",
            height=6, data=mpg)

