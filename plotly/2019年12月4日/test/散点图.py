import plotly.graph_objects as go
import numpy as np
import plotly.express as px
import pandas as pd

# np.log10(x)
# get data
data = pd.read_csv("./huoshan.xls", sep="\t", header=0, index_col=0)
data['padj'] = np.log10(data['padj']) * (-1)

fig = px.scatter(data, x="log2FoldChange", y="padj", color="level")
fig.show()
