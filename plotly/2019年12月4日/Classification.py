import plotly.graph_objects as go

import pandas as pd

# db1 = pd.read_csv('/home/ubuntu/jupyter_notebook/data/classification.csv')
db1 = pd.read_csv('./classification.csv')

fig = go.Figure()

fig.add_trace(go.Sunburst(
    ids=db1.ids,
    labels=db1.labels,
    parents=db1.parents,
    values=db1.value,
    branchvalues="total",
))

fig.update_layout(margin=dict(t=0, l=0, r=0, b=0))
fig.show()



from plotly.offline import plot
figure = {'data': [{'x': [0, 1], 'y': [0, 1]}],
          'layout': {'xaxis': {'range': [0, 5], 'autorange': False},
                     'yaxis': {'range': [0, 5], 'autorange': False},
                     'title': 'Start Title'},
          'frames': [{'data': [{'x': [1, 2], 'y': [1, 2]}]},
                     {'data': [{'x': [1, 4], 'y': [1, 4]}]},
                     {'data': [{'x': [3, 4], 'y': [3, 4]}],
                      'layout': {'title': 'End Title'}}]}
plot(figure, animation_opts={'frame': {'duration': 1}})