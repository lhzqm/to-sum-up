import plotly.graph_objects as go
import plotly.figure_factory as ff
import pandas as pd
import plotly.express as px
from plotly.offline import plot

import numpy as np
from scipy.spatial.distance import pdist, squareform
from scipy.cluster.hierarchy import linkage


# # Parameters
# width = 800
# height = 800
# # 对称的
# # symmetric = False
# symmetric = True

# # Currently must be True
# col_cluster = True

# row_cluster = True
# row_anno = True


# data_file = "./3.tsv"
# col_anno_file = "3.meta.tsv"
# row_anno_file = "3.meta.tsv"

# data_file = "./ExpRawDataETABM84AAFFY44.tab"
# col_anno_file = "colAnno.txt"
# row_anno_file = "rowAnno.txt"


# colAnno.txt 就是我们的metadata,是我们传进去的

# rowanno 在 sample correlation 那一步等同于colanno

# END Parameters
#
def hot_map(data_file, col_anno_file, row_anno_file='', width=800, height=800, symmetric=True):

    # Currently must be True
    col_cluster = True

    # Parameter check
    if col_anno_file:
        col_anno = True

    row_anno = True
    if row_anno_file:
        row_anno = True

    if symmetric:
        height = width
        row_cluster = col_cluster
        if row_anno and not row_anno_file:
            row_anno_file = col_anno_file
    else:
        width = height / 1.5
        width = height
    # ----------------------------

    if row_anno and not row_anno_file:
        assert 1 == 2, "Row anno file must be specified"
    # End Parameter check

    # get data
    data = pd.read_csv(data_file, sep="\t", header=0, index_col=0)

    # Only for test
    data = data.head(20)

    num_row, num_col = data.shape

    # height = width * num_row / num_col

    # Data check
    # if symmetric:
    # #     对称 矩阵
    #    assert num_row == num_col, "Non symmetric matrix"

    # Create column dendrogram, required in current version
    # 创建列树状图，当前版本中需要
    col_dendrogram = ff.create_dendrogram(
        data.T, orientation='bottom', labels=data.columns)
    col_dendro_leaves = list(col_dendrogram['layout']['xaxis']['ticktext'])

    for i in range(len(col_dendrogram['data'])):
        col_dendrogram['data'][i]['yaxis'] = 'y2'

    # Check if row cluster need
    if row_cluster:
        row_dendrogram = ff.create_dendrogram(
            data, orientation='left', labels=data.index)

        # 设置坐标轴，用于排布图，想排布多少个就加多少个坐标轴
        # 对应于fig.update_layout(xaxis2）
        # 如果是x3，对应于 xaxis3
        for i in range(len(row_dendrogram['data'])):
            row_dendrogram['data'][i]['xaxis'] = 'x2'

        # 数据整合到底板图
        for tmp_data in row_dendrogram['data']:
            col_dendrogram.add_trace(tmp_data)

        row_dendro_leaves = list(row_dendrogram['layout']['yaxis']['ticktext'])
        # 统一Y轴的坐标，这个在排图时很关键
        # tickvals是控制谁跟谁对其的关键
        col_dendrogram['layout']['yaxis']['tickvals'] = row_dendrogram['layout']['yaxis']['tickvals']
    else:
        row_dendro_leaves = list(data.index)
        col_dendrogram['layout']['yaxis']['tickvals'] = list(
            range(-5, (-10)*len(row_dendro_leaves)-5, -10))

    # change yaxis label
    col_dendrogram['layout']['yaxis']['ticktext'] = row_dendro_leaves
    fig = col_dendrogram

    # Create Heatmap
    heat_data = data.loc[row_dendro_leaves, col_dendro_leaves]

    heatmap = [
        go.Heatmap(
            x=col_dendro_leaves,
            y=row_dendro_leaves,
            z=heat_data,
            colorscale='blues'
        )
    ]

    # tickvals的使用，x，y对应到对应坐标
    heatmap[0]['x'] = col_dendrogram['layout']['xaxis']['tickvals']
    heatmap[0]['y'] = col_dendrogram['layout']['yaxis']['tickvals']

    # Add Heatmap Data to Figure
    for tmp_data in heatmap:
        fig.add_trace(tmp_data)

    # Add col_anno
    if col_anno:
        # col heatmap
        col_heatmap_y_axis = 'y3'
        colData = pd.read_csv(col_anno_file, sep="\t", header=0, index_col=0)

        # 最开始是尝试给第3个坐标轴，结果大小不可控
        # 新方案是把列注释跟热图整合一起，人为修改横轴坐标，把这个数据加到热图中
        # col_anno_y_dict 这个字典就是存坐标的
        colData_column_names = list(colData.columns)
        ymin = col_dendrogram['layout']['yaxis']['tickvals'][0]
        ymin_next = col_dendrogram['layout']['yaxis']['tickvals'][1]
        increase = ymin - ymin_next
        col_anno_y_dict = {}
        for tmp_name in colData_column_names:
            ymin += increase
            col_anno_y_dict[tmp_name] = ymin

        colData_select = colData.loc[col_dendro_leaves, ]

        assert colData_select.shape[0] == num_col, "Number of row in col anno file is not the same number of columns in data file"

        colData_select['index_col'] = colData_select.index
        colData_select['xvals'] = col_dendrogram['layout']['xaxis']['tickvals']
        colData_stack = colData_select.melt(
            id_vars=["index_col", "xvals"], var_name="anno")
        # colData_stack.head()

        # 因为下面改了坐标y，这里加一个额外注释列，用于后面的customdata
        # customdata也可以包含很多值
        col_anno_scatter = px.scatter(colData_stack, x="index_col", y="anno",
                                      color="value", hover_name="anno", hover_data=["value", "index_col"])
        col_anno_scatter_data = col_anno_scatter['data']
        for col_anno_scatter_tmp in col_anno_scatter_data:
            # 最开始是尝试给第3个坐标轴，结果大小不可控
            # 新方案是把列注释跟热图整合一起，人为修改横轴坐标，把这个数据加到热图中
            # col_anno_y_dict 这个字典就是存坐标的
            #col_anno_scatter_tmp['yaxis'] = col_heatmap_y_axis
            col_anno_scatter_tmp['y'] = [col_anno_y_dict[i]
                                         for i in col_anno_scatter_tmp['y']]
            col_anno_scatter_tmp['x'] = colData_select.loc[col_anno_scatter_tmp['x'], 'xvals'].to_list(
            )
            # 更改鼠标悬浮文字
            col_anno_scatter_tmp['hovertemplate'] = '<b>%{hovertext}: </b>%{customdata[1]} - %{customdata[0]}'
        # col_anno_scatter_data

        # Add Heatmap Data to Figure
        for tmp_data in col_anno_scatter_data:
            fig.add_trace(tmp_data)
    # --End col_anno-------------------------

    # Add row_anno
    if row_anno:
        row_heatmap_x_axis = 'x3'
        rowData = pd.read_csv(row_anno_file, sep="\t", header=0, index_col=0)

        rowData_column_names = list(rowData.columns)
        xmax = col_dendrogram['layout']['xaxis']['tickvals'][-1]
        xmax_next = col_dendrogram['layout']['xaxis']['tickvals'][-2]
        increase = xmax - xmax_next
        row_anno_x_dict = {}
        for tmp_name in rowData_column_names:
            xmax += increase
            row_anno_x_dict[tmp_name] = xmax

        rowData_select = rowData.loc[row_dendro_leaves, ]
        assert rowData_select.shape[0] == num_row, "Number of row in row anno file is not the same number of columns in data file"

        rowData_select['index_row'] = rowData_select.index

        rowData_select['yvals'] = col_dendrogram['layout']['yaxis']['tickvals']
        rowData_stack = rowData_select.melt(
            id_vars=["index_row", "yvals"], var_name="anno")
        # rowData_stack.head()

        # 注意横纵轴转换
        row_anno_scatter = px.scatter(rowData_stack, x="anno", y="index_row",
                                      color="value", hover_name="anno", hover_data=["value", "index_row"])
        row_anno_scatter_data = row_anno_scatter['data']
        for row_anno_scatter_tmp in row_anno_scatter_data:
            #row_anno_scatter_tmp['xaxis'] = row_heatmap_x_axis
            row_anno_scatter_tmp['y'] = rowData_select.loc[row_anno_scatter_tmp['y'], 'yvals'].to_list(
            )
            row_anno_scatter_tmp['x'] = [row_anno_x_dict[i]
                                         for i in row_anno_scatter_tmp['x']]
            row_anno_scatter_tmp['hovertemplate'] = '<b>%{hovertext}: </b>%{customdata[1]} - %{customdata[0]}'

        # row_anno_scatter_data

        # Add Heatmap Data to Figure
        for tmp_data in row_anno_scatter_data:
            fig.add_trace(tmp_data)
    # --End row_anno-------------------------

    # Edit Layout
    fig.update_layout({'width': width, 'height': height,
                       'showlegend': False, 'hovermode': 'closest',
                       })

    if row_cluster:
        fig.update_layout(xaxis={'domain': [0, 0.85],
                                 'mirror': False,
                                 'showgrid': False,
                                 'showline': False,
                                 'zeroline': False,
                                 'ticks': ""})
        # Edit xaxis2
        fig.update_layout(xaxis2={'domain': [0.86, 1],
                                  'mirror': False,
                                  'showgrid': False,
                                  'showline': False,
                                  'zeroline': False,
                                  'showticklabels': False,
                                  'ticks': ""})
    else:
        fig.update_layout(xaxis={'domain': [0, 1],
                                 'mirror': False,
                                 'showgrid': False,
                                 'showline': False,
                                 'zeroline': False,
                                 'ticks': ""})

    # -------------------------------------

    # Edit yaxis
    fig.update_layout(yaxis={'domain': [0, .85],
                             'mirror': False,
                             'showgrid': False,
                             'showline': False,
                             'zeroline': False,
                             'showticklabels': True,
                             'ticks': "outside"
                             })
    # Edit yaxis2
    fig.update_layout(yaxis2={'domain': [.86, .975],
                              'mirror': False,
                              'showgrid': False,
                              'showline': False,
                              'zeroline': False,
                              'showticklabels': False,
                              'ticks': ""})
    # ----------------------------------------

    # Plot!
    fig.show()
#     plot_div = plot(fig, output_type='div', )

#     with open('{}.txt'.format(data_file), 'w', encoding='utf-8') as f:
#         f.write(plot_div)

#     return plot_div


# Parameters
# width = 800
# height = 800
# 对称的
# symmetric = False
# symmetric = True

# Currently must be True
col_cluster = True

row_cluster = True
# row_anno = True


data_file = "3.tsv"
col_anno_file = "3.meta.tsv"
# row_anno_file = "3.meta.tsv"



hot_map(data_file, col_anno_file)