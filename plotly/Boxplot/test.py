# # import plotly.express as px
# # df = px.data.tips()
# # fig = px.box(df, x="day", y="total_bill", points="all")
# # fig.show()
# #
# import plotly.graph_objects as go
# fig = go.Figure(
#     data=[go.Bar(y=[2, 1, 3])],
#     layout_title_text="A Figure Displayed with the 'svg' Renderer"
# )
# fig.show(renderer="pdf")

import plotly.graph_objects as go

fig = go.Figure(
    data=[go.Bar(y=[2, 1, 3])],
    layout_title_text="A Figure Displayed with the 'svg' Renderer"
)
fig.show(renderer="./test.svg")

