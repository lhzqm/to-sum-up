import pandas as pd
from plotly.offline import plot
import plotly.graph_objects as go

db1 = pd.read_csv('classification.csv')

fig = go.Figure()

fig.add_trace(go.Sunburst(
    ids=db1.ids,
    labels=db1.labels,
    parents=db1.parents,
    values=db1.value,
    branchvalues="total",
))

fig.update_layout(margin=dict(t=0, l=0, r=0, b=0))

fig.show()

plot_div = plot(fig, output_type='div', include_plotlyjs='directory')
