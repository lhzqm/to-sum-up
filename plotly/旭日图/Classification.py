import plotly.graph_objects as go

import pandas as pd

db1 = pd.read_csv('/home/ubuntu/jupyter_notebook/data/classification.csv')

fig =go.Figure()

fig.add_trace(go.Sunburst(
    ids=db1.ids,
    labels=db1.labels,
    parents=db1.parents,
    values=db1.value,
    branchvalues="total",
))

fig.update_layout(margin = dict(t=0, l=0, r=0, b=0))
fig.show()
