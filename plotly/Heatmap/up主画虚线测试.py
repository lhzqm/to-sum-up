import plotly.graph_objects as go


def show_raw_chart(how_to_display):
    numbers = list(range(10))
    fig = go.Figure()
    fig.add_trace(go.Scatter(x=numbers, y=[x ** 2 for x in numbers]))
    fig.add_trace(go.Scatter(x=numbers, y=[x ** 3 for x in numbers]))
    if how_to_display == "browser":
        fig.show()
    elif how_to_display == "image":
        fig.write_image("./" + "raw_line_chart.svg")
    else:
        print("do nothing")


bg_color = '#343434'
legend_color = "#ffffff"
grid_color = '#696969'
title_font = dict(color="#ffffff", size=24)
axis_title_font = dict(color="#ffffff", size=16)
line_colors = ['#fbd017', '#f48301']
DIR = "./"


def show_customized_chart(how_to_display="browser"):
    numbers = list(range(10))

    title = "<b>" + "Showing Trends" + "</b>"
    x_title = "X axis"
    y_title = "Y axis"
    fig = go.Figure(layout=_layout(title, x_title, y_title))

    fig.update_layout(legend_orientation="h")

    fig.update_layout(
        shapes=[
            _vertical_line(),
            _rectangle()
        ]
    )
    # 线一
    fig.add_trace(
        go.Scatter(
            x=numbers,
            y=[x ** 2 for x in numbers],
            mode="lines",
            name="lines",
            line=dict(color=line_colors[0])
        )
    )
    # 线二
    fig.add_trace(
        go.Scatter(
            x=numbers,
            y=[x ** 3 for x in numbers],
            mode="lines+markers",
            name="lines+markers",
            line=dict(color=line_colors[1])
        )
    )

    if how_to_display == "browser":
        fig.show()
    else:
        fig.write_image(DIR + "test.svg")


def _layout(title, x_title, y_title):
    legend_setting = dict(
        traceorder="normal",
        font=dict(
            color=legend_color,
            size=16
        )
    )

    title_setting = dict(
        text=title,
        pad=dict(
            t=-5,
            l=-5,
            r=-5
        ),
        font=title_font
    )

    return go.Layout(
        xaxis=_axis(x_title, range=None),
        yaxis=_axis(y_title, range=None),
        paper_bgcolor=bg_color,
        plot_bgcolor=bg_color,
        title=title_setting,
        showlegend=True,
        legend=legend_setting,
    )


def _axis(axis_name, range):
    return dict(
        gridcolor=grid_color,
        zerolinecolor=grid_color,
        showgrid=True,
        zeroline=True,
        title=axis_name,
        titlefont=axis_title_font,
        color="#696969",
        tickfont=dict(size=18),
        range=range
    )


def _vertical_line():
    #     图形垂直钱的设定
    line_setting = dict(color="#1478db", width=3, dash="dashdot")

    return go.layout.Shape(
        type="line",
        x0=8,
        y0=0,
        x1=8,
        y1=700,
        line=line_setting
    )


def _rectangle():
    return go.layout.Shape(
        type="rect",
        x0=0,
        y0=450,
        x1=10,
        y1=550,
        fillcolor="#5fba7d",
        opacity=0.2,
        layer="below",
        line_width=1,
    )
