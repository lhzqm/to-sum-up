import re
import numpy as np
import pandas as pd
import plotly.express as px
from plotly.offline import plot


def scatter_plot(data_file):
    # 读取文件
    data = pd.read_csv("{}".format(data_file), sep="\t", header=0, index_col=0)

    # 处理数据 log10*(-1)
    data['padj'] = np.log10(data['padj']) * (-1)

    # 绘图 color="level", color_discrete_map={'NoDiff': 'grey'} 设置 level 下的 NoDiff 数据为为灰色
    fig = px.scatter(data, x="log2FoldChange", y="padj",
                     color="level", color_discrete_map={'NoDiff': 'grey'})

    # 更改x轴标题
    fig.update_layout(xaxis=dict(title='Log2(Fold change)'))

    # 更改y轴标题
    fig.update_layout(yaxis=dict(title='-Log10(FDR)'))

    # 更改图例位置
    fig.update_layout({'legend_orientation': 'h',
                       'legend_x': 0, 'legend_y': 1.1})

    # 更改图例显示文字
    fig.for_each_trace(lambda t: t.update(name=t.name.replace("level=", "")))

    # plot_bgcolor='#E6E6FA',# 图的背景颜色
    # fig.update_layout(plot_bgcolor='#FFFFFF')

    # paper_bgcolor='#F8F8FF',# 图像的背景颜色
    # fig.update_layout(paper_bgcolor='#efefef')

    # legend=dict(x=0.5,y=0.8,#设置图例的位置，[0,1]之间 
    # font=dict(family='sans-serif',size=26,color='black'),#设置图例的字体及颜色 
    # gcolor='#E2E2E2',bordercolor='#FFFFFF'),#设置图例的背景及边框的颜色
    # fig.update_layout(legend=dict(bgcolor='#E2E2E2',bordercolor='#FFFFFF'))

    # 更改图像尺寸
    # fig.update_layout(autosize=False, width=int(width), height=int(height))

    # 更改图像的边距
    # fig.update_layout(margin=go.Margin(t=10))

    # 生成div include_plotlyjs
    # plot_div = plot(fig, output_type='div', include_plotlyjs='directory')
    
    # 绘图
    fig.show()
    return fig


fig = scatter_plot("./Scatter_plot_data.xls")